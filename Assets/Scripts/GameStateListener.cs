﻿using UnityEngine;
using System.Collections;

public interface GameStateListener {
	void OnGameStarted ();
	void OnGamePaused ();
	void OnGameResumed ();
	void OnGameEnded ();
	void OnGameExit ();
	void OnGameRestart ();
}
