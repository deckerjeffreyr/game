﻿using UnityEngine;
using System.Collections;

public class UIManagerController : MonoBehaviour, GameStateListener {
	
	public static UIManagerController instance;
	
	public GameObject gameMenu;
	public GameObject pauseMenu;
	public GameObject settingsMenu;
	public GameObject hud; 
	public GameObject gameOverMenu;

	public GameManagerController gameManagerController;

	// Use this for initialization
	void Start () {
		instance = this;
		ShowGameMenu ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
//	Hide and Show Methods for Game UI
	
	void ShowGameMenu () {
		if (gameMenu == null) {
			GameObject gameMenuResource = Resources.Load("GameMenu") as GameObject;
			// Get the menu object here
			// Set click listeners and default text here
			gameMenu = Instantiate(gameMenuResource);
		}
		gameMenu.SetActive (true);
	}
	
	void HideGameMenu () {
		if (gameMenu != null) {
			Destroy(gameMenu);
		}
	}
	
	void ShowSettings () {
		
	}
	
	void HideSettings () {
		
	}
	
	void ShowPauseMenu () {
		
	}
	
	void HidePauseMenu () {
	
	}
	
	void ShowHUD () {
		if (hud == null) {
			GameObject hudResource = Resources.Load("hud") as GameObject;
			// Get the menu object here
			// Set click listeners and default text here
			hud = Instantiate(hudResource);
		}
		hud.SetActive (true);
	}
	
	void HideHUD () {
		if (hud != null) {
			Destroy(hud);
		} 
	}
	
	void ShowGameOver () {
		if (gameOverMenu == null) {
			GameObject gameOverMenuResource = Resources.Load("game_over_menu") as GameObject;
			// Get the menu object here
			// Set click listeners and default text here
			gameOverMenu = Instantiate(gameOverMenuResource);
		}
		gameOverMenu.SetActive (true);
	}
	
	void HideGameOver () {
		gameOverMenu.GetComponent<ScoreMenuController>().Remove();
	}
//	Button Click Handlers
	
	// Play click to start game from game menu
	public void OnPlayClick () {
		gameManagerController.StartGame ();
	}
	
	// Pause click to pause current game
	public void OnPauseClick () {
		gameManagerController.PauseGame ();
	}
	
	// Restart click to restart ended game
	public void OnRestartClick () {
		gameManagerController.RestartGame();
	}
	
	// Resume click ot resmue current game
	public void OnResumeClick () {
		gameManagerController.ResumeGame ();
	}
	
	// Settings click to display settings from game menu
	public void OnSettingsClick () {
		HideGameMenu ();
		ShowSettings  ();
	}
	
	// Mute click to mute/unmute game music
	public void OnMuteMusicClick () {
		// Mute Game Music here
	}
	
	// Mute click to mute/unmute game sound fx
	public void OnMuteSoundFXClick () {
		// Mute Sound FX here
	}
	
	// Game State Listener Impl
	
	public void OnGameStarted() {
		HideGameMenu ();
		ShowHUD ();
	}
	
	public void OnGamePaused() {
		HideHUD ();
		ShowPauseMenu ();
	}
	
	public void OnGameResumed() {
		HidePauseMenu ();
		ShowHUD ();
	}
	
	public void OnGameEnded() {
		HideHUD();
		HidePauseMenu();
		ShowGameOver();
	}
	
	public void OnGameExit () {
		HideGameOver();
		ShowGameMenu();
	}
	
	public void OnGameRestart () {
		HideGameOver();
	}
}
