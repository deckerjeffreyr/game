﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {

	GameObject playButton;
	GameObject settingsButton;

	// Use this for initialization
	void Start () {
		GetComponent<Canvas>().worldCamera = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void Remove () {
		Destroy (gameObject);
	}
}
