﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelSpawnController : MonoBehaviour, GameStateListener {

	public List<string> platformPrefabs;
	public List<string> obstaclePrefabs;
	public List<GameObject> activePrefabInstances;
	public List<GameObject> activeObstacleInstances;
	public float speed = 3.5f;
	public float obstacleFrequency = 4f;
	public float projectileFrequency = 2f;
	public float platformCubeSize = 0.5f;
	
	private float obstacleStartTime;
	private float lastObstacleTime;
	private float lastProjectileTime;
	
	private bool buildingPlatform = false;
	
	// Use this for initialization
	void Start () {
		activePrefabInstances = new List<GameObject>();
		activeObstacleInstances = new List<GameObject>();
		ResetLevel();
	}
	
	// Update is called once per frame
	void Update () {
		CreateRandomPlatform ();
		CreateRandomProjectile();
    }
    
    void FixedUpdate () {
		MoveGameObjects(activePrefabInstances);
		MoveGameObjects(activeObstacleInstances);
    }
    
    void MoveGameObjects (List<GameObject> gos) {
		List<GameObject> removeList = new List<GameObject>();
		
		// loop through the game objects to update their positions
		foreach (GameObject o in gos) {
			if (o==null) {
				removeList.Add (o);
				continue;
			}
			
			Move (o.transform);
		}
		
		foreach (GameObject o in removeList) {
			gos.Remove (o);
		}
		
		removeList.Clear ();
		removeList = null;
    }
    
    void StartObstacleGeneration () {
    	obstacleStartTime = Time.realtimeSinceStartup + 5;
    }
    
    void StartProjectileGeneration () {
    	lastProjectileTime = Time.realtimeSinceStartup;
    }
    
    void Move(Transform t) {
		t.position += new Vector3(0, (-1 * speed * Time.deltaTime));
    }
    
    void CreateStartingPlatform () {
    	float spawnBegin = -Camera.main.orthographicSize -2.75f;
    	bool addPlatformBlock = true;
		GameObject newPlatformResource = null;
		GameObject lastPlatformRendered = null;
        
    	while (addPlatformBlock) {
            int randPlatform = Random.Range(0, platformPrefabs.Count);
            string platformName = platformPrefabs[randPlatform];
            newPlatformResource = Resources.Load(platformName) as GameObject;
            Vector3 spawnLocation = new Vector3 (0f, (spawnBegin + (newPlatformResource.GetComponent<Renderer>().bounds.size.y / 2)));
            lastPlatformRendered = Instantiate(newPlatformResource, spawnLocation, new Quaternion ()) as GameObject;
            activePrefabInstances.Add (lastPlatformRendered);
            spawnBegin = lastPlatformRendered.GetComponent<Renderer>().bounds.max.y;
            
			if (spawnBegin >= Camera.main.orthographicSize) addPlatformBlock = false;
        }
    }
    
    
	void CreateRandomPlatform () {
		// Get platform prefab
		GameObject lastPlatformBlock = null;
		if (activePrefabInstances.Count > 0) {
			lastPlatformBlock = activePrefabInstances[activePrefabInstances.Count - 1];
		}		

		if (buildingPlatform) return;
		
        if (lastPlatformBlock != null && lastPlatformBlock.GetComponent<Renderer>().isVisible) {
            buildingPlatform = true;
            
            int platformSize = Random.Range(2, 10);
            int blockCount = (int)(platformSize / .5);
            
            Vector3 lastPlatformEnd = lastPlatformBlock.GetComponent<Renderer>().bounds.max;
            
            // where our new platform of blocks should begin
            GameObject newPlatformResource = null;
            GameObject lastPlatformRendered = null;
            
            if (IsPlaying()) {
				Vector3 spawnBegin = new Vector3 (0f, lastPlatformEnd.y + 2.5f);
				float platformBegin = spawnBegin.y;
                for (int i = 0; i < blockCount; i++) {
					int randPlatform = Random.Range(0, platformPrefabs.Count);
					string platformName = platformPrefabs[randPlatform];
					newPlatformResource = Resources.Load(platformName) as GameObject;
					Vector3 spawnLocation = new Vector3 (0f, (spawnBegin.y + (newPlatformResource.GetComponent<Renderer>().bounds.size.y / 2)));
					lastPlatformRendered = Instantiate(newPlatformResource, spawnLocation, new Quaternion ()) as GameObject;
					activePrefabInstances.Add (lastPlatformRendered);
					spawnBegin = lastPlatformRendered.GetComponent<Renderer>().bounds.max;
                }
				float platformEnd = lastPlatformRendered.GetComponent<Renderer>().bounds.max.y;
				
				CreateRandomObstacle(platformBegin, platformEnd);
			} else {
				Vector3 spawnBegin = new Vector3 (0f, lastPlatformEnd.y);
                int randPlatform = Random.Range(0, platformPrefabs.Count);
				string platformName = platformPrefabs[randPlatform];
				newPlatformResource = Resources.Load(platformName) as GameObject;
				Vector3 spawnLocation = new Vector3 (0f, (spawnBegin.y + (newPlatformResource.GetComponent<Renderer>().bounds.size.y / 2)));
				lastPlatformRendered = Instantiate(newPlatformResource, spawnLocation, new Quaternion ()) as GameObject;
				activePrefabInstances.Add (lastPlatformRendered);
				spawnBegin = lastPlatformRendered.GetComponent<Renderer>().bounds.max;
        	}
        
            buildingPlatform = false;	
		}
    }
    
    void CreateRandomObstacle (float platformStart, float platformEnd) {
		// Add Obstacle
		if (IsPlaying() && obstacleStartTime > 0 && obstacleStartTime <= Time.realtimeSinceStartup && (lastObstacleTime + obstacleFrequency) <= Time.realtimeSinceStartup) {
			float platformLength = platformEnd - platformStart;
			
			int randObstacle = Random.Range(0, obstaclePrefabs.Count);
			string obstacleName = obstaclePrefabs[randObstacle];
			GameObject newObstacle = Resources.Load(obstacleName) as GameObject;
			
			// Determine spawn point
			float obstacleLength = newObstacle.GetComponent<Obstacle>().BoundsRenderer().bounds.size.y;
			float spawnRange = platformLength - obstacleLength;
			
			float ySpawnPoint = Random.Range(0, spawnRange) + (obstacleLength/2) + platformStart;
			float xSpawnPoint = -newObstacle.GetComponent<Obstacle>().SpawnDelta(platformCubeSize);
			
			Vector3 rotation = newObstacle.transform.rotation.eulerAngles;
			
			// Determines if obstacle is on left or right
			int side = Random.Range(0,2);
			if (side == 1) {
				print ("--------> Rotating:"+xSpawnPoint);
				xSpawnPoint *= -1;
				print ("--------> Rotated:"+xSpawnPoint);
				rotation = new Vector3(rotation.x, rotation.y, rotation.z + 180);
			}
			
			Vector3 obstacleSpawnLocation = new Vector3 (xSpawnPoint, ySpawnPoint );
			print ("---------> spawnLocation:"+obstacleSpawnLocation);
			activeObstacleInstances.Add (Instantiate(newObstacle, obstacleSpawnLocation, Quaternion.Euler(rotation)) as GameObject);
			lastObstacleTime = Time.realtimeSinceStartup;
		}
    
    }
    
    void CreateRandomProjectile () {
		// Add Obstacle
		if (IsPlaying() && lastProjectileTime > 0 && (lastProjectileTime + projectileFrequency) <= Time.realtimeSinceStartup) {
			string projectileName = "asteroid";
			GameObject newProjecile = Resources.Load(projectileName) as GameObject;
			
			Bounds projectileBounds = newProjecile.GetComponent<Obstacle>().BoundsRenderer().bounds;
			// Determine spawn point
			float projectileLength = projectileBounds.size.y;
			float maxCameraY = Camera.main.orthographicSize + Camera.main.transform.position.y;
			float ySpawnPoint = projectileLength + maxCameraY;
			float xSpawnPoint = -newProjecile.GetComponent<Obstacle>().SpawnDelta(platformCubeSize);
			
			Vector3 rotation = newProjecile.transform.rotation.eulerAngles;
			// TODO: make this a method
			int side = Random.Range(0,2);
			if (side == 1) {
				print ("--------> Rotating:"+xSpawnPoint);
				xSpawnPoint *= -1;
				print ("--------> Rotated:"+xSpawnPoint);
				rotation = new Vector3(rotation.x, rotation.y, rotation.z + 180);
			}
			
			Vector3 projectileSpawnLocation = new Vector3 (xSpawnPoint, ySpawnPoint );
			print ("---------> spawnLocation:"+projectileSpawnLocation);
			activeObstacleInstances.Add (Instantiate(newProjecile, projectileSpawnLocation, Quaternion.Euler(rotation)) as GameObject);
            lastProjectileTime = Time.realtimeSinceStartup;
        }
    }
    
    public void ResetLevel() {
    	ClearLevel();
    	CreateStartingPlatform();
    }
    
    public void ClearLevel() {
    	foreach (GameObject o in activePrefabInstances) {
    		Destroy(o);
    	}
    	
    	foreach (GameObject o in activeObstacleInstances) {
    		Destroy(o);
    	}
    	
    	activePrefabInstances.Clear();
    	activeObstacleInstances.Clear();
    }
    
    public bool IsPlaying() {
		return GameManagerController.instance.gameState == GameManagerController.GameState.PLAYING;
    }
    
//    GameStateListener Method Implementations
    
    public void OnGameStarted () {
		StartObstacleGeneration();
		StartProjectileGeneration();
    }
    
    public void OnGamePaused () {
    
    }
    
    public void OnGameResumed () {
    
    }
    
    public void OnGameEnded () {
    }
    
	public void OnGameExit () {
	
	}
	
	public void OnGameRestart () {
	
	}
}
