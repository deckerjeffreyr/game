﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour, GameStateListener {

	public int score = 0; // Player score
	public bool isInverted = false;
	public float jumpForce = 5f;
	
	private bool shouldJump = false;
	private float lastX;
	private int platformsTouched = 0;
	private int platformsLeft = 0;
	
	private Vector3 startPosition = new Vector3(-0.57f, -3f);
	
	// Use this for initialization
	void Start() {
		lastX = transform.position.x;
		GetComponent<Rigidbody2D>().isKinematic = true;
		MoveToStart();
	}
		
	// Update is called once per frame
	void Update() {
		CheckForInput ();
	}

	void FixedUpdate() {
		Jump();
		SwitchGravity();
		UpdateCamera();
	}
	
	void CheckForInput() {
		if (Input.GetKeyDown(KeyCode.Space)) {
			shouldJump = true;
		}
		
		if (Input.touchCount > 0)
		{
			print("SCREEN HAS BEEN TOUCHED TOUCHED TOUCHED");
			//Store the first touch detected.
			shouldJump = Input.touches[0].phase == TouchPhase.Began;
		}
		
		if (Input.GetMouseButtonDown(0)) {
			shouldJump = true;
		}
		
	}
	
	void Jump() {
		int jumpModifier = -1;
		if (Physics2D.gravity.x < 0) jumpModifier = 1;
		
		if (shouldJump && CanJump()) {
            GetComponent<Rigidbody2D>().AddForce(transform.right * jumpModifier * jumpForce, ForceMode2D.Impulse);
        }
        shouldJump = false;
    }
    
    bool CanJump() {
        return platformsTouched != platformsLeft;
    }
    
    void SwitchGravity() {
        if ((lastX > 0 && transform.position.x <= 0) || lastX < 0 && transform.position.x >= 0) {
            Physics2D.gravity = Physics2D.gravity * -1;
            print("Gravity switvhing to "+Physics2D.gravity);
		}
		lastX = transform.position.x;
	}
	
	void UpdateCamera() {
		if (Camera.main == null) return;
		Camera.main.transform.position = transform.TransformPoint (0f, 2f, -10); 
	}
	
//	Collision Handling
	
	void OnCollisionExit2D(Collision2D col) {
		if (col.gameObject.tag == "Platform") {
			platformsLeft +=1;
			
			if (platformsLeft >= 1000) {
				platformsTouched = 0;
				platformsLeft = 0;
			}
		}
	}
	
	void OnCollisionEnter2D(Collision2D col) {
		if (col.gameObject.tag == "Platform") {
			platformsTouched+=1;
		}
	}
	
// 	Trigger Handling
	void OnTriggerEnter2D(Collider2D col) {
		if (col.gameObject.tag == "Obstacle") {
			GameManagerController.instance.EndGame();
        }
	}
	
//	GameStateListener Method Implementations
	
	public void OnGameStarted() {
		GetComponent<Rigidbody2D>().isKinematic = false;
		
	}
	
	public void OnGamePaused() {
		GetComponent<Rigidbody2D>().isKinematic = true;
	}
	
	public void OnGameResumed() {
		GetComponent<Rigidbody2D>().isKinematic = false;
	}
	
	public void OnGameEnded() {
		GetComponent<Rigidbody2D>().isKinematic = true;
	}
	
	
	public void OnGameExit () {
		
	}
	
	public void OnGameRestart () {
		
	}
	
	// TODO: Needed Animations
	// Rotate to vertical
	// Rotate to horrizontal
	// Move to start
	
	public void MoveToStart() {
		gameObject.transform.position = startPosition;
//		Vector3 animationTarget = startPosition;
//		float moveIncrement = -.05f;
//		float waitTime = .01f;
//		
//		while (GetComponent<Transform>().position != animationTarget) {
//			Vector3 position = GetComponent<Transform>().position;
//			float moveX = position.x == animationTarget.x ? 0 : moveIncrement;
//			float moveY = position.y == animationTarget.y ? 0 : moveIncrement;
//			
//			GetComponent<Transform>().position += new Vector3(moveX, moveY);
//			
//			if (GetComponent<Transform>().position.x < animationTarget.x) {
//				GetComponent<Transform>().position.Set(animationTarget.x, 
//					GetComponent<Transform>().position.y, 
//					GetComponent<Transform>().position.z);
//			}
//			
//			if (GetComponent<Transform>().position.y <= animationTarget.y) {
//				GetComponent<Transform>().position.Set(GetComponent<Transform>().position.x, 
//					animationTarget.y, 
//					GetComponent<Transform>().position.z);
//			}
//			
//			yield return new WaitForSeconds(waitTime);
//		}
	}
}
