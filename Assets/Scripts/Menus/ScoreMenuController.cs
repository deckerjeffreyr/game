﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreMenuController : MonoBehaviour {

	public GameObject restartButton;
	public GameObject exitButton;

	// Use this for initialization
	void Start () {
		GetComponent<Canvas>().worldCamera = Camera.main;
		
		UnityEngine.Events.UnityAction restartButtonAction = () => {GameManagerController.instance.RestartGame();};
		restartButton.GetComponent<Button>().onClick.AddListener(restartButtonAction);
		
		UnityEngine.Events.UnityAction exitButtonAction = () => {GameManagerController.instance.ExitGame();};
		exitButton.GetComponent<Button>().onClick.AddListener(exitButtonAction);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Remove () {
		Destroy (gameObject);
	}
}
