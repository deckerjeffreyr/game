﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HudController : MonoBehaviour {

	public GameObject pauseButton;
	public GameObject scoreValue;

	// Use this for initialization
	void Start () {
		GetComponent<Canvas>().worldCamera = Camera.main;
		
		UnityEngine.Events.UnityAction pauseButtonAction = () => {GameManagerController.instance.PauseGame();};
		pauseButton.GetComponent<Button>().onClick.AddListener(pauseButtonAction);
		
		scoreValue.GetComponent<Text>().text = "0";
	}
	
	// Update is called once per frame
	void Update () {
		scoreValue.GetComponent<Text>().text = GameManagerController.instance.player.score.ToString();
	}
}
