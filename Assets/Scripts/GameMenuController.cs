﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameMenuController : MonoBehaviour {

	public GameObject playButton;
	
	// Use this for initialization
	void Start () {
		GetComponent<Canvas>().worldCamera = Camera.main;
		
		UnityEngine.Events.UnityAction playButtonAction = () => {GameManagerController.instance.StartGame();};
		playButton.GetComponent<Button>().onClick.AddListener(playButtonAction);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void Remove () {
		Destroy (gameObject);
    }
}
