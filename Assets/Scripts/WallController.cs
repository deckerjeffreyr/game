﻿using UnityEngine;
using System.Collections;

public class WallController : MonoBehaviour {

	public bool isRotating = false;
	public bool isShifting = false;
	public bool isSliding = false;
	
	public float rotationSpeed = 90f;
	public float shiftSpeed = 2f;
	public float slideSpeed = 2f;
	
	public float shiftTripTime = 2f;
	public float slideTripTime = 2f;
	
	public float shiftStartTime = 0f;
	public float slideStartTime = 0f;
	
	public float lastShiftSwitch = 0f;
	public float lastSlideSwitch = 0f;
	
	public float shiftModifier = 1f;
	public float slideModifier = 1f;

	// Use this for initialization
	void Start () {
		gameObject.tag = "Obstacle";
	}
	
	// Update is called once per frame
	void Update () {
	
		if (isShifting) {
			transform.Translate((transform.right * shiftSpeed * shiftModifier * Time.deltaTime));
		}
		
		if (isSliding) {
			transform.Translate((transform.up * slideSpeed * slideModifier * Time.deltaTime));
		}
	}
	
	// 
	void FixedUpdate () {
		if (isShifting) {
			DoShift ();
		}
		
		if (isSliding) {
			DoSlide ();
		}
		
		if (isRotating) {
			transform.Rotate(new Vector3(0f, 0f, -(rotationSpeed * Time.fixedDeltaTime)));
        }
	}
	
	void DoShift () {
	
		if (shiftStartTime == 0f) {
			shiftStartTime = Time.time;
		}
		
		if (lastShiftSwitch == 0f) {
			if ((Time.time - shiftStartTime) >= (shiftTripTime/2)) {
				shiftModifier = shiftModifier * -1;
				lastShiftSwitch = Time.time;
			}
		} else {
			if ((Time.time - lastShiftSwitch) >= shiftTripTime) {
				shiftModifier = shiftModifier * -1;
                lastShiftSwitch = Time.time;
            }
        }
    }
    
    void DoSlide () {
    
		if (slideStartTime == 0f) {
			slideStartTime = Time.time;
		}
		
		if (lastSlideSwitch == 0f) {
			if ((Time.time - slideStartTime) >= (slideTripTime/2)) {
				slideModifier = slideModifier * -1;
				lastSlideSwitch = Time.time;
			}
		} else {
			if ((Time.time - lastSlideSwitch) >= slideTripTime) {
				slideModifier = slideModifier * -1;
                lastSlideSwitch = Time.time;
            }
        }
    }
}
