﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManagerController : MonoBehaviour {
	
	public static GameManagerController instance = null;

	public enum GameState {
		STOPPED,
		PAUSED,
		PLAYING
	}

	public GameState gameState = GameState.STOPPED;
	
	public PlayerController player;
	public LevelSpawnController levelSpawn;
	public UIManagerController uiManager;
	
	public List<GameObject> stateDelegates;
	// Use this for initialization
	void Start () {
		instance = this;
		GameObject playerObject = Instantiate(Resources.Load("player"), new Vector3(0,0), new Quaternion ()) as GameObject;
		player = playerObject.GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void FixedUpdate () {
	
	}
	
	public void StartGame () {
		gameState = GameState.PLAYING;
		player.OnGameStarted ();
		levelSpawn.OnGameStarted ();
		uiManager.OnGameStarted();
	}
	
	public void PauseGame () {
		gameState = GameState.PAUSED;
	}
	
	public void RestartGame() {
		player.MoveToStart();
		levelSpawn.ResetLevel();
		uiManager.OnGameRestart();
		StartGame();
	}
	
	public void ResumeGame () {
		gameState = GameState.PLAYING;
	}
	
	public void EndGame () {
		gameState = GameState.STOPPED;
		player.OnGameEnded();
		levelSpawn.OnGameEnded();
		uiManager.OnGameEnded();
	}
	
	public void ExitGame () {
		Application.LoadLevel (Application.loadedLevelName);
	}
	
	
}
