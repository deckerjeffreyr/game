﻿using UnityEngine;
using System.Collections;

public class PlatformController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		gameObject.tag = "Platform";
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnBecameInvisible () {
		// Destroy the game object after the prefab has left the screen
		print("destroying prefab level object");
		Destroy(gameObject);
	}
}
