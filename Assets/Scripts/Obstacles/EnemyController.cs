﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour, Obstacle {

	public GameObject platform;
	public float traverseSpeed = 2f;

	private int traverseModifier = 1;

	// Use this for initialization
	void Start () {
		gameObject.tag = "Obstacle";
	}
	
	// Update is called once per frame
	void Update () {
		Traverse();
	}
	
	void Traverse () {
		if (platform == null) return;
	
		Bounds myBounds = GetComponent<Renderer>().bounds;
		float myMinY = myBounds.min.y;
		float myMaxY = myBounds.max.y;
		
		Bounds platformBounds = GetComponent<Renderer>().bounds;
		float platformMinY = platformBounds.min.y;
		float platformMaxY = platformBounds.max.y;
		
		if (myMinY <= platformMinY || myMaxY >= platformMaxY) {
			traverseModifier*=-1;
		}
	
		transform.Translate((transform.up * traverseSpeed * traverseModifier * Time.deltaTime));
	}
	
	void OnTriggerEnter2D (Collider2D col) {
		if (col.gameObject.tag == "Player") {
			// Perform kill animation
		}
	}
	
	public bool IsDangerous() {
		return true;
	}
	
	public float SpawnDelta(float platformWidth) {
		return (gameObject.GetComponent<Renderer>().bounds.size.x + platformWidth)/2;
	}
	
	public Renderer BoundsRenderer() {
		return gameObject.GetComponent<Renderer>();
	}
	
	
	// Destroys the object when after its no longer visible
	void OnBecameInvisible () {
		Destroy(gameObject);
	}
}
