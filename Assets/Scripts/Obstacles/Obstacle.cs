﻿using UnityEngine;
using System.Collections;

public interface Obstacle {
	// Will the player die when they touch it
	bool IsDangerous();
	// How far away from the center should the obstacle spawn
	float SpawnDelta(float platformWidth);
	// Retruns the renderer used used for spawning location
	Renderer BoundsRenderer();
}
