﻿using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour, Obstacle {

	public float speed = 3f;
	public Quaternion angle; // Not used for now but maybe we'll do projectiles traveling at different angles
	private int rotationDirection = 1;

	// Use this for initialization
	void Start () {
		gameObject.tag = "Obstacle";
		
		int rotation = Random.Range(0,2);
		if (rotation == 1) {
			rotationDirection = -1;
		}
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(new Vector3(0, 0, 180) * Time.deltaTime * rotationDirection, Space.World);
		transform.Translate(new Vector3(0, (-1 * speed)) * Time.deltaTime, Space.World);
	}
	
	public bool IsDangerous() {
		return true;
	}
	
	public float SpawnDelta(float platformWidth) {
		return (gameObject.GetComponent<Renderer>().bounds.size.x + platformWidth + .5f)/2;
	}
	
	public Renderer BoundsRenderer() {
		return gameObject.GetComponent<Renderer>();
	}
	
	void OnBecameInvisible () {
		Destroy(gameObject);
	}
}
