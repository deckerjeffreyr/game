﻿using UnityEngine;
using System.Collections;

public class ObstacleController : MonoBehaviour, Obstacle {

	// Use this for initialization
	void Start () {
		gameObject.tag = "Obstacle";
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public bool IsDangerous() {
		return true;
	}
	
	public float SpawnDelta(float platformWidth) {
		return (gameObject.GetComponent<Renderer>().bounds.size.x + platformWidth)/2;;
	}
	
	public Renderer BoundsRenderer() {
		return gameObject.GetComponent<Renderer>();
	}
	
	public static Bounds OrthographicBounds(Camera camera)
	{
		float screenAspect = (float)Screen.width / (float)Screen.height;
		float cameraHeight = camera.orthographicSize * 2;
		Bounds bounds = new Bounds(
			camera.transform.position,
			new Vector3(cameraHeight * screenAspect, cameraHeight, 0));
		return bounds;
	}
	
	void OnBecameInvisible () {
//	float y = Camera.main.GetComponent<Renderer>().bounds.center.y;
//		Bounds cameraBounds = OrthographicBounds(Camera.main);
//		if (cameraBounds.min.y >= gameObject.GetComponent<Renderer>().bounds.max.y) {
			Destroy(gameObject);
//		}
		
	}
}
