﻿using UnityEngine;
using System.Collections;

public class GeyserController : MonoBehaviour, Obstacle {

	public float eruptInterval = 2f;
	public float eruptDurration = .5f;
	public float nextEruption = 0;
	public bool isErupting = false;
	public GameObject gooSpout;
	public GameObject crater;
	
	private GeyserController pairedGeyser;
	private float stopErupting = 0;

	// Use this for initialization
	void Start () {
		gameObject.tag = "Obstacle";
		gooSpout.GetComponent<Renderer>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void FixedUpdate () {
		if (gooSpout == null && crater == null) {
			OnBecameInvisible();
			return;
		}
	
		if (Time.fixedTime >= nextEruption) {
			isErupting = true;
			gooSpout.GetComponent<Renderer>().enabled = true;
			nextEruption+=eruptInterval;
			stopErupting = Time.fixedTime + eruptDurration;
		} else if (isErupting && Time.fixedTime >= stopErupting) {
			isErupting = false;
			gooSpout.GetComponent<Renderer>().enabled = false;
			print(gooSpout);
		}
	}
	
	public void SetPairedGeyser(GeyserController pair) {
		pairedGeyser = pair;
		pairedGeyser.nextEruption = nextEruption + (eruptInterval / 2);
	}
	
	public bool IsDangerous() {
		return isErupting;
	}
	
	public float SpawnDelta(float platformWidth) {
		return (gooSpout.GetComponent<Renderer>().bounds.size.x + platformWidth)/2;;
	}
	
	public Renderer BoundsRenderer() {
		return crater.GetComponent<Renderer>();
	}
	
	void OnBecameInvisible () {
		Destroy(gameObject);
		Destroy(gooSpout);
		Destroy(crater);
	}
}
